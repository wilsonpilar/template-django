#!/bin/bash

# Script to set up a Django project on Vagrant.

# Installation settings

PROJECT_NAME=$1

DB_NAME=$PROJECT_NAME
PROJECT_DIR=/home/vagrant/$PROJECT_NAME
PGSQL_VERSION=9.4

echo "export LANGUAGE=en_US.UTF-8" >> /etc/bash.bashrc
echo "export LANG=en_US.UTF-8" >> /etc/bash.bashrc
echo "export LC_ALL=en_US.UTF-8" >> /etc/bash.bashrc

source /etc/bash.bashrc

locale-gen en_US.UTF-8
dpkg-reconfigure locales
echo "[STEP] #1"
export LANGUAGE=en_US.UTF-8
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8
echo "[STEP] #2"
# Install essential packages from Apt
apt-get update -y
echo "[STEP] #3"
# Python dev packages
apt-get install -y build-essential python python-dev python-pip
echo "[STEP] #4"
apt-get install -y python-setuptools
echo "[STEP] #5"
apt-get install -y libjpeg-dev libtiff-dev zlib1g-dev libfreetype6-dev liblcms2-dev
echo "[STEP] #6"
apt-get install -y git
echo "[STEP] #7"
# Postgresql
apt-get install -y postgresql-$PGSQL_VERSION libpq-dev
echo "[STEP] #8"
PG_CONF=/etc/postgresql/$PGSQL_VERSION/main/pg_hba.conf

echo "local   all         postgres                          trust" >  $PG_CONF
echo "local   all         all                               trust" >> $PG_CONF
echo "host    all         all         127.0.0.1/32          trust" >> $PG_CONF

/etc/init.d/postgresql reload

echo "[STEP] #9"
apt-get install -y nodejs
echo "[STEP] #10"
apt-get install -y npm
echo "[STEP] #11"
# postgresql setup for project
createdb -Upostgres $DB_NAME
echo "[STEP] #12"
pip install -r $PROJECT_DIR/requirements.txt
echo "[STEP] #13"

chmod a+x $PROJECT_DIR/manage.py
echo "[STEP] #14"
su - vagrant -c "cd $PROJECT_DIR && ./manage.py migrate"
echo "[STEP] #15"