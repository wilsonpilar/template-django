-r requirements.txt

mox==0.5.3
ipdb
model-mommy==1.2.2
PyYAML==3.11
mock==1.0.1