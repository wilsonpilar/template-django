# README #

Template com configuração default para projetos com Django 1.8 e PostgreSQL


Criar novo projeto:


```
#!bash


django-admin startproject meuprojeto --template https://bitbucket.org/wilsonpilar/template-django/get/master.zip -n Vagrantfile

vagrant up (apenas na primeira vez que acessa o projeto)

vagrant ssh 

cd meuprojeto

python manage.py runserver 0.0.0.0:8000

```


Depois acessar via browser http://127.0.0.1:8000/